#
# Description: <Method description here>
#
require 'savon'

begin
  
  server=$evm.root["ipam_server"]
  user=$evm.root["ipam_user"]
  password=$evm.root["ipam_password"]
  
  prov=$evm.root["miq_provision"]
  vmname=prov.get_option(:vm_name)
  subnet=prov.get_option(:subnet)
  device_type=$evm.object["device_type"] #"Virtual Machine CMP"
  
  service="IncUseNextReservedIPAddress"
  action=:use_next_reserved_ip_address
  url="http://#{server}/inc-ws/services/#{service}"
  $evm.log(:info, "CC- user=#{user} password=#{password} URL=#{url}")
  client=Savon.client(wsdl: "#{url}?wsdl", basic_auth: [user,password], log: true, log_level: :debug, pretty_print_xml: true) rescue nil
  $evm.log(:info, "CC- #{action}, message: {inpDevice: {device_type: #{device_type}, ip_address: #{subnet}, hostname: #{vmname}}})")
  response = client.call(action, message: {inpDevice: {device_type: device_type, ip_address: subnet, hostname: vmname}}) rescue nil
  
  ip=response.body[:use_next_reserved_ip_address_response][:use_next_reserved_ip_address_return] rescue nil
  raise ("no IP availlable for #{vmname} from #{subnet}") if ip.nil?
  
  $evm.log(:info,"CC- setting IP for #{vmname} to #{ip}")
#    prov.set_nic_settings(0,{:ip_addr => "1.1.1.1"})

  prov.set_nic_settings(0,{:ip_addr => "#{ip}"}) unless ip.nil?
  $evm.log(:info, " CC- ip set to #{prov.get_option(:ip_addr)} ")
  $evm.root['ae_result']         = "ok"

rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
