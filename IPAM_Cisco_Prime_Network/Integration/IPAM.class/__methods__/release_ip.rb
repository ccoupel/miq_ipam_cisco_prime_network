#
# Description: <Method description here>
#
require 'savon'

begin
  $evm.root.attributes.each{|k,v| $evm.log(:info,"CC- ROOT[#{k}]=#{v}")}
  
  server=$evm.root["ipam_server"]
  port=$evm.root["ipam_port"]
  user=$evm.root["ipam_user"]
  password=$evm.root["ipam_password"]
  device_type=$evm.object["device_type"] #"Virtual Machine CMP"

  vm=$evm.root["vm"]
  if vm.nil?
          $evm.log(:info,"CC- VMnot defined, using miq_provision")
    prov=$evm.root["miq_provision"]
    ip_address=prov.get_option(:ip_addr) rescue nil

  else
    ip_address=vm.ipaddresses.first rescue nil
    if ip_address.blank?
      $evm.log(:info,"CC- #{vm.name} has no IP, using miq_provision")
      prov=vm.miq_provision 
      if prov.nil?
        $evm.root['ae_result']         = "ok"
        $evm.log(:warn,"CC- #{vm.name} has no miq_provision")
      end
      ip_address=prov.get_option(:ip_addr) rescue nil
    end
  end
  $evm.log(:info,"CC- releasing #{ip_address} from IPAM")

  unless ip_address.blank?
   service="Deletes"
    action=:delete_device
    url="http://#{server}/inc-ws/services/#{service}"
    $evm.log(:info, "CC- user=#{user} password=#{password} URL=#{url}")
    client=Savon.client(wsdl: "#{url}?wsdl", basic_auth: [user,password], log:true, log_level: :debug, pretty_print_xml: true)
    $evm.log(:info, "CC- #{action}, message: {inpDevice: {ip_address: #{ip_address}}}")
    response = client.call(action, message: {inpDevice: {ip_address: ip_address}})
  
    service="Imports"
    action=:import_device
    url="http://#{server}/inc-ws/services/#{service}"
    $evm.log(:info, "CC- user=#{user} password=#{password} URL=#{url}")
    client=Savon.client(wsdl: "#{url}?wsdl", basic_auth: [user,password], log:true, log_level: :debug, pretty_print_xml: true)
    $evm.log(:info, "CC- #{action}, message: {inpDevice: {ip_address: #{ip_address}, address_type: \"Reserved\", device_type: #{device_type}}}")
    response = client.call(action, message: {inpDevice: {ip_address: ip_address, address_type: "Reserved", device_type: device_type}})

    $evm.root['ae_result']         = "ok"
  end
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
